const path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const {clientRules} = require("./clientrules");

module.exports = {
    entry: {
        index: [
            './src/client/index.js',
        ]
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: "/"
    },
    mode: 'development',
    devtool: "inline-source-map",
    stats: {
        assets: false,
        modules: false,
        chunks: false,
        children: false,
        hash: false,
        entrypoints: false,
        version: false,
    },
    module: {
        rules: clientRules
    },
    plugins: [
        new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'Fine Arts Center',
            template: "./src/template/index.ejs",
            filename: "./index.html",
            react_id: 'react-entry'
        })
        /*
        new CompressionPlugin({
            filename: '[path].gz[query]',
            test: /\.(js|css|html|svg)$/,
            compressionOptions: {level: 2},
            threshold: 10240,
            minRatio: 0.8,
            deleteOriginalAssets: true,
        })*/
    ]
};