const path = require('path');
const webpack = require("webpack");
const TerserPlugin = require('terser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const {clientRules} = require("./clientrules");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const contextPath = path.resolve(__dirname, '../');

module.exports = {
    context: contextPath,
    entry: './src/client/index.js',
    output: {
        filename: '[name].[contenthash].bundle.js',
        chunkFilename: "[name].[contenthash].bundle.js",
        path: path.resolve(contextPath, 'dist'),
        publicPath: "/"
    },

    mode: 'production',
    module: {
        rules: clientRules
    },
    optimization: {
        minimizer: [new TerserPlugin()],
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    enforce: true,
                    chunks: 'all'
                }
            }
        }
    },
    plugins: [
        new webpack.DefinePlugin({ // <-- key to reducing React's size
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.AggressiveMergingPlugin(),//Merge chunks
        new CleanWebpackPlugin(),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new HtmlWebpackPlugin({
            title: 'Fine Arts Center',
            template: "./src/template/index.ejs",
            filename: "./index.html",
            react_id: 'react-entry'
        }),
        new webpack.HashedModuleIdsPlugin(),
        new CompressionPlugin({
            filename: '[path].br[query]',
            algorithm: 'brotliCompress',
            test: /\.(js|css|html|svg)$/,
            compressionOptions: {level: 11},
            threshold: 10240,
            minRatio: 0.8,
            deleteOriginalAssets: true,
        }),
        //new BundleAnalyzerPlugin(),
    ]
};