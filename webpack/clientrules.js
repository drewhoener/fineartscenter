module.exports.clientRules = [
    {
        test: /\.m?js(x)?$/,
        exclude: /(node_modules|bower_components)/,
        use: {
            loader: 'babel-loader',
            options: {
                babelrc: true
            }

        }
    },
    {
        test: /\.(woff(2)?|otf|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
            {
                loader: 'file-loader',
                options: {
                    name: './font/[name].[ext]',
                },
            },
        ]
    },
    {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
            {
                loader: 'file-loader',
                options: {
                    name: './img/[name].[ext]',
                },
            },
        ]
    },
    {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
    }
];