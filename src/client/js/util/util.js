import {Responsive} from "semantic-ui-react";

export const mobileWidth = {
    minWidth: Responsive.onlyMobile.minWidth,
    maxWidth: Responsive.onlyTablet.maxWidth
};

export const evalError = (message) => {
    return !(message === null || message.length <= 0);
};