import React from "react";
import {connect} from "react-redux";
import {Button, Form, Segment} from "semantic-ui-react";
import {setLoginVisible} from "../../state/action/types/loginaction";
import {setFormField, submitNewUser} from "../../state/action/types/userformaction";

const Home = ({setVisible, logState, visible, firstName, lastName, phone, email, setField, createUser}) => (
    <>
        <Button type='button' onClick={() => {
            console.log(visible);
            setVisible(!visible);
            logState();
        }} content='Visibility'/>
        <Segment>
            <Form onSubmit={createUser}>
                <Form.Input label='First Name' type='text' name='firstName' value={firstName} onChange={setField}/>
                <Form.Input label='Last Name' type='text' name='lastName' value={lastName} onChange={setField}/>
                <Form.Input label='Phone Number' type='text' name='phone' value={phone} onChange={setField}/>
                <Form.Input label='Email' type='text' name='email' value={email} onChange={setField}/>
                <Button type='submit' content={'Submit Form'}/>
            </Form>
        </Segment>
    </>
);

const mapStateToProps = (state) => ({
    visible: state.loginForm.visible,
    firstName: state.userForm.firstName,
    lastName: state.userForm.lastName,
    phone: state.userForm.phone,
    email: state.userForm.email,
    logState: () => console.log(state)
});

const mapDispatchToProps = (dispatch) => ({
    setVisible: (state) => {
        console.log(`Got State ${state}`);
        dispatch(setLoginVisible(state))
    },
    setField: (e, {name, value}) => dispatch(setFormField(name, value)),
    createUser: () => dispatch(submitNewUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
