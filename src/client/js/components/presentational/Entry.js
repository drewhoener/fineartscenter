import React from 'react';
import PopupLoginSegment from "./login/PopupLoginSegment";
import Home from "./Home";

const Entry = () => (
    <>
        <div style={({zIndex: 0, position: 'absolute'})}>
            <Home/>
        </div>
        <PopupLoginSegment/>
    </>
);

export default Entry;