import {connect} from "react-redux";
import React from "react";
import {Segment} from "semantic-ui-react";
import LoginForm from "../../container/login/LoginForm";
import posed, {PoseGroup} from "react-pose";

const Popup = posed.div({
    enter: {
        y: 0,
        opacity: 1,
        delay: 300,
        transition: {
            y: {type: 'spring', stiffness: 500, damping: 15},
            default: {duration: 700}
        }
    },
    exit: {
        y: -500,
        opacity: 0,
        transition: {duration: 700}
    }
});

const PopupLoginSegment = ({visible}) => (
    <div className={'overlayFormWrapper'}>
        <PoseGroup>
            {
                visible && [
                    <Segment as={Popup} key='popup-login-segment' className='loginForm' size='massive'>
                        <LoginForm/>
                    </Segment>
                ]
            }
        </PoseGroup>
    </div>

);

const mapStateToProps = (state) => ({
    visible: state.loginForm.visible
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(PopupLoginSegment);