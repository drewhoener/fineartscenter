import {connect} from "react-redux";
import {Button, Form, Grid, Header, Image, Message, Responsive} from "semantic-ui-react";
import React from "react";
import umassImage from "../../../../img/FacLogo.png";
import {beginLogin, setPassword, setUsername} from "../../../state/action/types/loginaction";
import {evalError} from "../../../util/util";

const formSubmit = (e, data, onSubmit) => {
    e.preventDefault();
    onSubmit();
};

const LoginForm = ({
                       username, password, loading, errorVisible, errorMessage,
                       onUsernameChange, onPasswordChange, printState, onSubmit
                   }) => {
    return (
        <Grid verticalAlign={'middle'} stackable>
            <Grid.Row>
                <Grid.Column width={16}>
                    <Header textAlign={'center'} size={'medium'} content={'UMass FAC Login'}/>
                </Grid.Column>
            </Grid.Row>
            <Grid.Row>
                <Responsive as={Grid.Column} verticalAlign={'middle'} width={6}
                            minWidth={Responsive.onlyTablet.minWidth}>
                    <Image src={umassImage} inline fluid/>
                </Responsive>
                <Grid.Column computer={10} tablet={10} mobile={16}>
                    <Form onSubmit={(e, d) => formSubmit(e, d, onSubmit)} className={'noAsterisk'} loading={loading}>
                        <Form.Input autoComplete='on' label='Username' name='username' type='text' value={username}
                                    onChange={onUsernameChange} readOnly={loading} required/>
                        <Form.Input autoComplete='on' label='Password' name='password' type='password'
                                    value={password} onChange={onPasswordChange} readOnly={loading} required/>
                        <Button fluid color='red' type='submit' content='Login'/>
                        <Button fluid color='red' type='button' content='Log State' onClick={printState}/>
                        <Message error header='Login Error' content={errorMessage} visible={errorVisible}/>
                    </Form>
                </Grid.Column>
            </Grid.Row>
        </Grid>

    );
};

const mapStateToProps = (state) => ({
    username: state.loginForm.username,
    password: state.loginForm.password,
    loading: state.loginForm.loading,
    errorVisible: evalError(state.loginForm.message),
    errorMessage: state.loginForm.message,
    printState: () => {
        console.log(state);
        console.log(JSON.stringify(state, null, 4));
    }
});

const mapDispatchToProps = (dispatch) => ({
    onUsernameChange: (e, d) => dispatch(setUsername(d.value)),
    onPasswordChange: (e, d) => dispatch(setPassword(d.value)),
    onSubmit: (data) => dispatch(beginLogin()),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);