import thunkMiddleware from 'redux-thunk';
import {applyMiddleware, createStore} from "redux";
import modState, {initialState} from "./reducer/reducers";

const store = createStore(modState, initialState, applyMiddleware(thunkMiddleware));

export {store};