import {combineReducers} from "redux";
import {modLoginForm} from "./types/loginreducers";
import {modUserForm} from "./types/userformreducers";

const initialState = {
    loginForm: {
        username: '',
        password: '',
        loading: false,
        visible: false,
        message: ''
    },
    userForm: {
        firstName: '',
        lastName: '',
        phone: '',
        email: ''
    }
};

const modState = combineReducers({
    loginForm: modLoginForm,
    userForm: modUserForm
});

export {initialState};
export default modState;