import {initialState} from "../reducers";
import ActionType from "../../action/actiontype";

export const modLoginForm = (state = initialState.loginForm, action) => {
    switch (action.type) {
        case ActionType.SET_USERNAME:
        case ActionType.SET_PASSWORD:
        case ActionType.SET_LOGIN_LOADING:
        case ActionType.SET_LOGIN_VISIBLE:
        case ActionType.SET_LOGIN_ERROR_MESSAGE:
            return Object.assign({}, state, {[action.key]: action[action.key]});
        case ActionType.RESET_FORM:
            return Object.assign({}, initialState.loginForm);
        default:
            return state;
    }
};