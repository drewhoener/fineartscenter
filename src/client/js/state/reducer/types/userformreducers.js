import {initialState} from "../reducers";
import ActionType from "../../action/types/userformaction";

export const modUserForm = (state = initialState.userForm, action) => {
    switch (action.type) {
        case ActionType.SET_USER_FORM_FIELD:
            return Object.assign({}, state, {[action.field]: action.val});
        default:
            return state;
    }
};