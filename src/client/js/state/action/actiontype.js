import {LoginActionTypes} from "./types/loginaction";
import UserFormActionTypes from "./types/userformaction";

const ActionType = {
    ...LoginActionTypes,
    ...UserFormActionTypes
};

export default ActionType;