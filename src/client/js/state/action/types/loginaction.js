import axios from "axios";

export const LoginActionTypes = {
    LOGIN_BEGIN: 'LOGIN_BEGIN',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAILURE: 'LOGIN_FAILURE',
    SET_USERNAME: 'SET_USERNAME',
    SET_PASSWORD: 'SET_PASSWORD',
    SET_LOGIN_LOADING: 'SET_LOGIN_LOADING',
    SET_LOGIN_VISIBLE: 'SET_LOGIN_VISIBLE',
    SET_LOGIN_ERROR_MESSAGE: 'SET_LOGIN_ERROR_MESSAGE',
    RESET_FORM: 'RESET_FORM',
};

export const beginLogin = () => (dispatch, getState) => {
    let {username, password} = getState().loginForm;
    dispatch(setLoginLoading(true));

    dispatch(setLoginError(''));
    axios.post('/login', {username, password})
        .then(response => {
            console.log('Found Response');
            setTimeout(() => {
                dispatch(setLoginLoading(false));
                dispatch(resetForm());
            }, 2000);
        })
        .catch(error => {
            let {status, data} = error.response;
            switch (status) {
                case 500:
                case 400:
                    dispatch(setLoginError('Unable to contact login servers'));
                    break;
                case 422:
                    dispatch(setLoginError(data.message));
                    break;
                default:
                    dispatch(setLoginError('Unspecified Error'));
                    break;
            }
            dispatch(setLoginLoading(false));
        });
};

export const loginSuccess = (data) => ({});

export const loginFailure = (data) => ({});

export const resetForm = () => dispatch => {
    dispatch(setLoginLoading(false));
    dispatch(setPassword(''));
    dispatch(setUsername(''));
};

export const setLoginError = (message) => ({
    type: LoginActionTypes.SET_LOGIN_ERROR_MESSAGE,
    message,
    key: 'message'
});

export const setLoginLoading = (state) => ({
    type: LoginActionTypes.SET_LOGIN_LOADING,
    loading: state,
    key: 'loading'
});

export const setLoginVisible = (state) => ({
    type: LoginActionTypes.SET_LOGIN_VISIBLE,
    visible: state,
    key: 'visible'
});

export const setUsername = (username) => ({
    type: LoginActionTypes.SET_USERNAME,
    username,
    key: 'username'
});

export const setPassword = (password) => ({
    type: LoginActionTypes.SET_PASSWORD,
    password,
    key: 'password'
});