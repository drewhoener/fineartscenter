import axios from "axios";

const UserFormActionTypes = {
    SET_USER_FORM_FIELD: "SET_USER_FORM_FIELD",
    USER_SUBMIT_BEGIN: 'USER_SUBMIT_BEGIN',
    USER_SUBMIT_SUCCESS: 'USER_SUBMIT_SUCCESS',
    USER_SUBMIT_FAILURE: 'USER_SUBMIT_FAILURE',
};

export const setFormField = (field, val) => ({
    type: UserFormActionTypes.SET_USER_FORM_FIELD,
    field,
    val
});

export const submitNewUser = () => (dispatch, getState) => {
    let {firstName, lastName, phone, email} = getState().userForm;
    axios.post('/login/createuser', {firstName, lastName, phone, email})
        .then(response => {

        })
        .catch(error => {
            let {response} = error;
            console.log(response);
        })
};

export default UserFormActionTypes;