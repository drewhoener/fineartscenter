require('./css/loginstyle.css');
require('./css/style.css');

import {render} from 'react-dom';
import {Provider} from "react-redux";
import {store} from "./js/state/store";
import React from "react";
import Login from "./js/components/presentational/Entry";

const entryPoint = document.getElementById('react-entry');
if (entryPoint) {
    render(
        <Provider store={store}>
            <Login/>
        </Provider>, entryPoint);
}
