import {Sequelize} from "sequelize";
import {getConfig} from "../util/pathutil";

const config = getConfig();
console.log(config.database);
console.log(config.dbuser);
console.log(config.dbpass);
const database = new Sequelize(config.dbname, config.dbuser, config.dbpass, {
    host: config.database,
    port: config.port,
    dialect: 'mariadb',
    dialectOptions: {connectTimeout: 1000} // mariadb connector option
});

export const isUserValid = (username, password) => {

};

export default database;