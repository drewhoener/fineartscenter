import User from "../model/User";
import {log} from "../../server";

export const getUser = (username) => {
    let user = User.findOne({where: {username}})
        .then(user => {
            return user;
        });
};

export const getUIDFromUsername = (username) => {
    let user = getUser(username);
    if (!user)
        return null;
    return user.uid;
};

export const createUser = (username, data) => {
    return User.sync()
        .then(() => {
            log.info('Synced table, finding/creating');
            return User.findOrCreate({where: {username}, defaults: data});
        }).then(([result, created]) => {
            log.info('Hello World');
            log.info(result.toJSON())
        });
};