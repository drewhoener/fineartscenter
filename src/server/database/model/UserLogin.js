import {DataTypes, Model} from "sequelize";
import {default as sequelize} from '../database';

class UserLogin extends Model {
    verifyPassword(password) {
        let salt = this.getDataValue('salt');
        let hash = this.getDataValue('hash');
        let newHash = crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha-1').toString('hex');
        return hash === newHash;
    }
}

UserLogin.init({
    uid: {
        type: DataTypes.UUIDV4,
        allowNull: false
    },
    hash: {
        type: DataTypes.STRING,
        allowNull: false
    },
    salt: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {sequelize, modelName: 'userlogin'});

export default UserLogin;