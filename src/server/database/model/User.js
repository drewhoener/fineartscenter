import {DataTypes, Model} from "sequelize";
import database from "../database";

export const getUsername = (firstName, lastName) => {
    if (lastName.length > 4)
        lastName = lastName.slice(0, 4);
    if (firstName.length > 4)
        firstName = firstName.slice(0, 4);
    return `${lastName}${firstName}`;
};

class User extends Model {
}

User.init({
    uid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },
    first_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    last_name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            is: /^(?:\+?\d\s?)?[2-9]\d{2}[- ]?\d{3}[- ]?\d{4}$/
        }
    },
    email: {
        type: DataTypes.STRING,
        validate: {
            isEmail: true
        }
    },
    psstaff: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    is_ta: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    }

}, {sequelize: database, modelName: 'users'});


export default User;
