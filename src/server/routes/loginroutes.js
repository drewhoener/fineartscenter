import express from 'express';
import {log} from "../server";
import {getUsername} from "../database/model/User";
import {createUser} from "../database/queries/userqueries";
import {hasFields} from "../util/utils";

const loginRouter = new express.Router();

loginRouter.post('/login', (req, res) => {
    log.info('Found Login Route');
    log.info(JSON.stringify(req.body, null, 4));
    res.status(422).json({message: 'Some Data'});
});

loginRouter.post('/login/createuser', async (req, res) => {
    if (!hasFields(['firstName', 'lastName', 'phone', 'email'], req.body)) {
        res.status(400).json({message: 'Missing required body components'});
        return;
    }
    let {firstName, lastName, phone, email} = req.body;
    log.info('Creating user in theory');
    let username = getUsername(firstName, lastName);
    await createUser(username, {
        username,
        first_name: firstName,
        last_name: lastName,
        phone: phone,
        email: email
    });
});

export default loginRouter;