import path from 'path';
import {generateCookieSecret} from "./crypt";
const fs = require('fs');

const file = path.resolve(__dirname, 'config.json');
console.log(file);
const defaultConfig = {
    secret: '',
    database: '127.0.0.1',
    port: '3306',
    dbuser: 'root',
    dbpass: '',
    dbname: 'fac'
};

export function getConfig(){
    if(!fs.existsSync(file))
        saveConfig(defaultConfig);
    const data = fs.readFileSync(file, 'utf8');
    const config = JSON.parse(data);
    if(!config.secret){
        config.secret = generateCookieSecret();
        saveConfig(config);
    }
    return config;
}

function saveConfig(configData){
    fs.writeFileSync(file, JSON.stringify(configData, null, 4));
}