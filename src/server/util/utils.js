export const hasFields = (fields, obj) => {
    if ((typeof fields) === 'string')
        fields = [fields];
    for (let str of fields) {
        if (!obj.hasOwnProperty(str))
            return false;
    }
    return true;
};