import crypto from 'crypto';

export function generateCookieSecret(){
    console.log('Generating random bytes for secret...');
    return crypto.randomBytes(20).toString('hex');
}