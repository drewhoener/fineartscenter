import express from 'express';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import logger from '../client/js/util/logging';
import {getConfig} from "./util/pathutil";
import session from 'express-session';
import loginRouter from "./routes/loginroutes";

//region constants
const serverLog = logger.extend('server');
const app = express(), STATIC = __dirname;
const expressStaticGzip = require('express-static-gzip');
const webpackConfig = require('../../webpack/webpack.rebuild.config.js');
const webpackCompiler = webpack(webpackConfig);
const bodyParser = require('body-parser');
//endregion

serverLog('Starting up');
const info = (message) => serverLog(message);
const log = {
    info
};

const config = getConfig();
log.info(JSON.stringify(config, null, 4));

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {    //Recompile on local changes so we don't have to rebuild
    serverLog('Using Webpack rebuild');
    app.use(webpackDevMiddleware(webpackCompiler, {
        quiet: true,
        noInfo: true,
        stats: {
            assets: false,
            modules: false,
            chunks: false,
            children: false,
            hash: false,
            entrypoints: false,
            version: false,
        },
        publicPath: webpackConfig.output.publicPath
    }));
    //use default static files
}

//parse request bodies
app.use(bodyParser.json());
//use cookies middleware
app.use(session({
    secret: config.secret,
    resave: false,
    saveUninitialized: true
}));

app.use('/', loginRouter);

//Respond to get requests, serve the base html file
app.get('*', expressStaticGzip(STATIC, {
    enableBrotli: true
}));

app.listen(3000, () => {
    //db.connect();
    serverLog("Express Server running on port 3000");
});

export {log};